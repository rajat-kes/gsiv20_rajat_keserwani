import React, { Component } from 'react';
import Header from './Component/Share/Header';
import MovieList from './Component/MovieList';
import MovieDetails from './Component/MovieDetails';
import { APIBASEURL, APIKEY } from './constent'
import BasicModal from './Component/Share/BasicModal';
class App extends Component {
  state = {
    movieList: [],
    page: 0,
    hasMore: true,
    isShowMoveDetail: false,
    selectedMovieId: null,
    movieDetails: {},
    basicModalObj:{
      open:false,
      titleMsg:'',
      disMsg:'',
    }
  }

  // hanle to show/hide details
  handleHideDetails = (isShowMoveDetail) => {
    this.setState({ isShowMoveDetail });
  }

  // handle card click to call API and get movie details
  handlerSetMovieDetailsId = ({ selectedMovieId }) => {
    this.setState({
      selectedMovieId
    }, () => { this.loadMovieDetails()});
  }

  handleBasicModal = (basicModalObj) => {
    this.setState({ basicModalObj });
  }

  // get movie details
  loadMovieDetails = () => {
    const { selectedMovieId } = this.state;
    fetch(`${APIBASEURL}/3/movie/${selectedMovieId}?api_key=${APIKEY}&language=en-US`)
      .then(response => response.json())
      .then(res => {
        if (res.id){
          this.setState({ movieDetails: res, isShowMoveDetail: true });
        } else if (!res.success) {
          this.handleBasicModal({
            isBasicModalOpen: true,
            titleMsg: 'API Error',
            disMsg: res.status_message || 'Details API getting Error',
          });
        }
      })
      .catch(err => { this.handleBasicModal({
        isBasicModalOpen: true,
        titleMsg: 'API Error',
        disMsg: err.error || 'Details API getting Error',
      });
    });
  }

  // get the list of movie
  loadMovieList = () => {
    const { page, movieList } = this.state;
    fetch(`${APIBASEURL}/3/movie/512/lists?api_key=${APIKEY}&language=en-US&page=${page + 1}`)
      .then(response => response.json())
      .then(res => {
        let hasMore= true;
        if (res.page === res.total_pages){
          hasMore= false;
        }
        this.setState({
          movieList: [...movieList, ...res.results],
          hasMore,
          page: res.page,
        });
      })
      .catch(err => {
        this.handleBasicModal({
          isBasicModalOpen: true,
          titleMsg: 'API Error',
          disMsg: err.error || 'List API getting Error',
        });
      });
  }

  render() {
    const {
      isShowMoveDetail,
      movieList,
      hasMore,
      movieDetails,
      basicModalObj
    } = this.state;
    return (
      <div>
        <Header
          showSearch={!isShowMoveDetail}
          showHeaderMsg={isShowMoveDetail}
          OnClickHome={() => this.handleHideDetails(false)}
        />
        <BasicModal
          open={basicModalObj.isBasicModalOpen}
          titleMsg={basicModalObj.titleMsg}
          disMsg={basicModalObj.disMsg}
          onClickClose={() => this.handleBasicModal({
            isBasicModalOpen: false,
            titleMsg: '',
            disMsg: '',
          })}
        />
        {isShowMoveDetail
          ? <MovieDetails
              movieDetails={movieDetails}
            />
          : <MovieList
              loadMovieList={this.loadMovieList}
              movieList={movieList}
              hasMore={hasMore}
              handlerSetMovieDetailsId={({selectedMovieId}) => this.handlerSetMovieDetailsId({ selectedMovieId })}
            />
        }
      </div>
    )
  }
}

export default App
