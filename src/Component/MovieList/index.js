import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import './MovieList.css';
import { IMGBASEURL, NOIMAGEPATH } from '../../constent';

function MovieList(props) {
  const { loadMovieList, movieList, hasMore = false, handlerSetMovieDetailsId } = props;
  let items = <li className="card noData">No Data</li>
  if (Array.isArray(movieList) && movieList.length > 0){
    items = movieList.map((data) =>
      <li
        className="card"
        key={data.id}
        onClick={() => { handlerSetMovieDetailsId({ selectedMovieId:data.id }) }}
      >
        <img
          src={
          (data && data.poster_path)
            ? `${IMGBASEURL}${data.poster_path}`
            : NOIMAGEPATH
          }
          alt="Poster Path"
        />
        <div className="title-wrapper">
          <span className="title">{data.name}</span>
          <span>(review)</span>
        </div>
        <span className="dis">{data.description}</span>
      </li>
    )
  }
  return (
    <div>
      <InfiniteScroll
        pageStart={0}
        loadMore={() => { if (loadMovieList) loadMovieList(); }}
        hasMore={hasMore}
        loader={<div className="loader" key={0}>Loading ...</div>}
      >
        <div className="main-list-wrapper">
          <ul className="card-wrapper">
            {items}
          </ul>
        </div>
      </InfiniteScroll>
    </div>
  )
}

export default MovieList
