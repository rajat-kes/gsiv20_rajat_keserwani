import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function BasicModal(props) {
  const {
    maxWidth = 'Xs',
    open= false,
    onClickClose,
    titleMsg,
    disMsg,
  } = props;

  const handleClose = () => {
    if (onClickClose) onClickClose();
  }

  return (
    <Dialog
      fullWidth={false}
      maxWidth={maxWidth}
      open={open}
      onClose={handleClose}
      aria-labelledby="max-width-dialog-title"
    >
      {titleMsg && <DialogTitle id="max-width-dialog-title">{titleMsg}</DialogTitle>}
      <DialogContent>
        <DialogContentText>
          {disMsg || ''}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Close
          </Button>
      </DialogActions>
    </Dialog>
  );
}
