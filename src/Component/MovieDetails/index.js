import React from 'react';
import { IMGBASEURL, NOIMAGEPATH } from '../../constent';
import './MovieDetails.css';

function MovieDetails(props) {
  const { movieDetails } = props;
  return (
    <div className="wrapper">
      <div>
        <img
          className="image"
          src={
            (movieDetails && movieDetails.poster_path)
              ? `${IMGBASEURL}${movieDetails.poster_path}`
              : NOIMAGEPATH
          }
          alt="Poster Path"
        />
      </div>
      <div className="data-wrapper">
        <div className="margin10px">
          <span className="title">{movieDetails.title}</span>{' '}
          <span className="rating">({movieDetails.vote_average})</span>
        </div>
        <div className="margin10px">Year | Length | Director</div>
        <div className="margin10px">Cost: Actor1 Actor2</div>
        <div className="margin10px line-height15rem">
          <span>Description:</span>{' '}
          <span>{movieDetails.overview}</span>
        </div>
      </div>
    </div>
  )
}

export default MovieDetails
